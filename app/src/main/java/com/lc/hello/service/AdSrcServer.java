package com.lc.hello.service;


import android.util.Log;

import com.lc.hello.utils.MyApplication;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

import fi.iki.elonen.NanoHTTPD;

/*
    使用介绍：
     try {
            AdSrcServer adSrcServer = new AdSrcServer();
            adSrcServer.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
*/
public class AdSrcServer extends NanoHTTPD {

    private static final String TAG = "KmH5Ad-server";

    public AdSrcServer(int port) {
        super(port);
    }

    public AdSrcServer() {
        super(8090);
    }

    /*接受响应的总方法
     * session里面包含所有的信息
     * 在此方法进行请求url方法判断*/
    @Override
    public Response serve(IHTTPSession session) {
        try {
            String uri = session.getUri().replace("//", "/");
            String relativePath[] = uri.split("/");
            Log.v(TAG, uri + " " + session.getMethod() + " " + relativePath[1]);

            switch (relativePath[1]) {
                case "file":
                    return responseFileStream(session, relativePath[2]);
                default:
                    return responseErrorPage(session, "default");
            }
        } catch (Exception e) {
            return responseErrorPage(session, e + "");
        }
    }

    private Response responseErrorPage(IHTTPSession session, String err) {
        StringBuilder builder = new StringBuilder();
        builder.append("<!DOCTYPE html><html><body>");
        builder.append("<h1>404 -- Sorry, Can't Found " + session.getUri() + " !</h1>");
        builder.append("<p>" + err + "</p>");
        builder.append("</body></html>");
        return newFixedLengthResponse(builder.toString());
    }


    public Response responseFileStream(IHTTPSession session, String filePath) {
        try {
            FileInputStream fis = new FileInputStream(MyApplication.FILE_PATH + filePath);
            return newChunkedResponse(Response.Status.OK, "*/*", fis);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return newFixedLengthResponse(Response.Status.NOT_FOUND, NanoHTTPD.MIME_PLAINTEXT, e.getMessage());
        }
    }

}
