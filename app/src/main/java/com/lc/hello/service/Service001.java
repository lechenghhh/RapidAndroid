package com.lc.hello.service;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;

/**
 * Created by Cheng on 2018/12/27.
 */

public class Service001 extends Service {
    public final static String ACTION_PRINTER_CONNECTED = "com.furi.Service001.Connected";
    public final static String ACTION_PRINTER_DISCONNECTED = "com.furi.Service001.Disonnected";
    public final static String ACTION_PRINTER_GET_STATE = "com.furi.Service001.GetState";
    public final static String ACTION_PRINTER_NOTICE_STATE = "com.furi.Service001.NoticeState";
    public final static String INTENT_IS_CONNECTED = "isConnected";
    public final static String INTENT_DATA = "Intentdata";

    public String data1 = "默认信息1";
    private boolean serviceRunning = false;
    private boolean isConnected = false;
    int i = 0;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        System.out.println("Service001-onBind" + serviceRunning);
        serviceRunning = true;
        return new Binder();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        System.out.println("Service001-onStartCommand" + serviceRunning);
        serviceRunning = true;
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        serviceRunning = true;
        System.out.println("Service001-onCreate" + serviceRunning);
        new Thread() {
            @Override
            public void run() {
                super.run();
                while (serviceRunning) {
                    System.out.println("Service001-onCreate2" + serviceRunning);
                    Intent intent = new Intent();
                    intent.setAction(ACTION_PRINTER_NOTICE_STATE);
                    intent.putExtra(INTENT_IS_CONNECTED, isConnected);
                    intent.putExtra(INTENT_DATA, i + "");
                    sendBroadcast(intent);
                    System.out.println("Service001-onCreate3 i=" + i);
                    try {
                        i += 3;
                        sleep(3000);
                    } catch (InterruptedException e) {
                    }
                }
            }
        }.start();
    }

    public BroadcastReceiver connectReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(ACTION_PRINTER_CONNECTED)) {
                intent.getStringExtra("");
            } else if (intent.getAction().equals(ACTION_PRINTER_DISCONNECTED)) {
                intent.getStringExtra("");
            } else if (intent.getAction().equals(ACTION_PRINTER_GET_STATE)) {//获取连接状态

            }
        }
    };

    @Override
    public void onDestroy() {
        super.onDestroy();
        System.out.println("Service001-onDestroy" + serviceRunning);
        serviceRunning = false;
    }
}
/*
//manifests 注册
        <service
            android:name=".service.Service001"
            android:enabled="true" />


//Activity操作
//启动并注册监听
    startService(new Intent(this, PrinterService.class));
                IntentFilter intentFilter = new IntentFilter();
                intentFilter.addAction(ACTION_PRINTER_NOTICE_STATE);
                registerReceiver(settingReceiver, intentFilter);
//

//接收消息并更新ui
    public BroadcastReceiver settingReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(ACTION_PRINTER_NOTICE_STATE)) {
                System.out.println("PrinterService-receiver" + intent.getStringExtra(INTENT_DATA));
                Message msg = new Message();
                msg.obj = intent.getStringExtra(INTENT_DATA);
                handler.sendMessage(msg);
            }
        }
    };

    public Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            tvPrinterState.setText(msg.obj.toString());
        }
    };

*/
