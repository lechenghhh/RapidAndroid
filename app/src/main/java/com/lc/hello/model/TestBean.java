package com.lc.hello.model;


import org.litepal.crud.DataSupport;

/**
 * Created by Cheng on 2018/9/7.
 */
public class TestBean extends DataSupport {

    private int id;
    private String num;
    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }

    /*
    //数据操作前必须加上
    SQLiteDatabase db = Connector.getDatabase();

    //增加

    Student student = new Student();
           student.setName("张三");
           student.setAge(19);
           student.setSex("男");
           student.setStudent_number("17000001");            student.save();
    //更新
    Student student = new Student();
            student.setAge(21);
            //更新名字为李四，且性别为男 学生的信息
            student.updateAll("name = ? and sex = ?","李四","男");

    //删除
    DataSupport.deleteAll(Student.class,"age < ?","18");

    //查询所有数据
    List<Student> list = DataSupport.findAll(Student.class);

    //select()  查询制定的列
    List<Student> list01 = DataSupport.select("name","age").find(Student.class);
    //where() 根据约束条件查询
    List<Student> list02 = DataSupport.where("age > ?","20").find(Student.class);
    //order() 对查询的结果进行排序 desc 表示降序排列 asc或者不写表示升序排列
    List<Student> list03 = DataSupport.order("student_number desc").find(Student.class);
    //limit() 指定查询结果的数量
    List<Student> list04 = DataSupport.limit(3).find(Student.class);
    //offset() 制定查询结果的偏移量 比如查询地2,3 条数据
    List<Student> list05 = DataSupport.offset(1).limit(2).find(Student.class);
    //以上的所有方法可以任意搭配组合，来完成复杂的查询操作
    List<Student> list06 = DataSupport.select("name","age","student_number")
        .where("age >?","20")
        .order("student_number")
        .limit(5)
        .find(Student.class);
     */

    @Override
    public String toString() {
        return "TestBean{" +
                "id=" + id +
                ", num='" + num + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}

