package com.lc.hello.Activity.unit;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.lc.hello.R;
import com.lc.hello.utils.MyApplication;
import com.lc.hello.utils.MyUtils;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CameraActivity extends BaseActivity {

    @Bind(R.id.btn1)
    Button btn1;
    @Bind(R.id.ivHead)
    ImageView ivHead;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);
        ButterKnife.bind(this);

    }

    //获取返回路径
    @OnClick({R.id.btn1, R.id.tv1})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn1:
                startActivityForResult(new Intent(MediaStore.ACTION_IMAGE_CAPTURE), 1);//申请成功，可以拍照
                break;
        }
    }

    @Override
    protected void onActivityResultWithPath(String srcPath) {
        super.onActivityResultWithPath(srcPath);
        Toast.makeText(this, "" + srcPath, Toast.LENGTH_SHORT).show();
    }
}
