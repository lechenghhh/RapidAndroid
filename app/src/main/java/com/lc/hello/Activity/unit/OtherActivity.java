package com.lc.hello.Activity.unit;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.lc.hello.R;
import com.lc.hello.service.AdSrcServer;
import com.lc.hello.utils.NetUtils;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.io.IOException;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class OtherActivity extends BaseActivity {

    @Bind(R.id.tv1)
    TextView tv1;
    @Bind(R.id.iv1)
    ImageView iv1;
    private AdSrcServer adSrcServer = null;
    private String adminUrl = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_other);
        ButterKnife.bind(this);
        adminUrl = "http://" + NetUtils.getIPAddress(this) + ":8090";
        tv1.append("访问地址: " + adminUrl + "\n");
        ImageLoader.getInstance().displayImage("http://qr.liantu.com/api.php?text=" + adminUrl, iv1);
    }

    @OnClick({R.id.tv2, R.id.tv1, R.id.iv1})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv1:
                new AlertDialog.Builder(OtherActivity.this).setTitle("这是标题")//设置对话框标题
                        .setMessage("这是消息")//设置显示的内容
                        .setCancelable(true)
                        .setPositiveButton("关闭整个界面", new DialogInterface.OnClickListener() {//添加确定按钮
                            @Override
                            public void onClick(DialogInterface dialog, int which) {//确定按钮的响应事件
                                finish();
                            }
                        }).setNegativeButton("仅关闭窗口", new DialogInterface.OnClickListener() {//添加返回按钮
                    @Override
                    public void onClick(DialogInterface dialog, int which) {//响应事件
                        Toast.makeText(OtherActivity.this, "仅关闭窗口", Toast.LENGTH_SHORT).show();
                    }
                }).show();//在按键
                break;
            case R.id.tv2:
                try {
                    if (adSrcServer == null) {
                        adSrcServer = new AdSrcServer();
                        adSrcServer.start();
                        Toast.makeText(this, "服务启动成功!", Toast.LENGTH_SHORT).show();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
            case R.id.iv1:
                startActivity(new Intent()
                        .setAction("android.intent.action.VIEW")
                        .setData(Uri.parse(adminUrl)));
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            adSrcServer.stop();
            adSrcServer = null;
        } catch (Exception e) {
        }
    }
}
