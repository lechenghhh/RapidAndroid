package com.lc.hello.Activity.unit;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;
import android.widget.Toast;

import com.lc.hello.R;


public class BrowserActivity extends AppCompatActivity implements OnClickListener {
    private WebView webView;
    private EditText etUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.aty018);

        init();
    }

    private void init() {
        setProgressBarVisibility(true);

        webView = (WebView) findViewById(R.id.webView);
        etUrl = (EditText) findViewById(R.id.etUrl);

        String url = getIntent().getStringExtra("url");
        if (url != null && !url.equals(""))
            etUrl.setText(url);
        findViewById(R.id.ivClose).setOnClickListener(this);
        findViewById(R.id.ivBack).setOnClickListener(this);
        findViewById(R.id.ivSync).setOnClickListener(this);
        findViewById(R.id.tvForward).setOnClickListener(this);
        findViewById(R.id.tvGoto).setOnClickListener(this);

        webView.setWebViewClient(new WebViewClient() {
            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                // handler.cancel();// Android默认的处理方式
                // handleMessage(Message msg);// 进行其他处理
                handler.proceed();//接受所有网站的证书
            }

            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (!url.startsWith("http")) {
                    Log.i("TAG", "处理自定义scheme");
                    Toast.makeText(BrowserActivity.this, "需要下载客户端收看", Toast.LENGTH_LONG).show();
                    try {// 以下固定写法
                        final Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        BrowserActivity.this.startActivity(intent);
                    } catch (Exception e) {
                        e.printStackTrace();                        // 防止没有安装的情况
                    }
                    return true;
                }
                view.loadUrl(url);
                etUrl.setText(url);
                return true;
            }
        });
        webView.setWebChromeClient(new WebChromeClient() {            // Set progress bar during loading
            public void onProgressChanged(WebView view, int progress) {
                BrowserActivity.this.setProgress(progress * 100);
            }
        });

        WebSettings websettings = webView.getSettings();        // Enable some feature like Javascript and pinch zoom
        websettings.setJavaScriptEnabled(true);                        // Warning! You can have XSS vulnerabilities!
        websettings.setBuiltInZoomControls(true);

        etUrl.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    switch (keyCode) {
                        case KeyEvent.KEYCODE_ENTER:
                            String thisUrl = completionUrl(etUrl.getText().toString());
                            webView.loadUrl(thisUrl);
                            etUrl.setText(thisUrl);
                            InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                            inputManager.hideSoftInputFromWindow(etUrl.getWindowToken(), 0);
                            return true;
                        default:
                            break;
                    }
                }
                return false;
            }
        });
        webView.loadUrl(completionUrl(etUrl.getText().toString()));
    }

    private String completionUrl(String thisUrl) {
        if (!thisUrl.contains("http"))
            thisUrl = "http://" + thisUrl;
        return thisUrl;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivClose:
                finish();
                break;
            case R.id.ivBack:
                if (webView.canGoBack()) webView.goBack(); // goBack()表示返回WebV
                else finish();
                break;
            case R.id.ivSync:
                webView.reload();
                break;
            case R.id.tvForward:
                webView.goForward();
                break;
            case R.id.tvGoto:
                webView.loadUrl(etUrl.getText().toString());
                break;
            /*case R.id.webview_stop:
                webView.stopLoading();
                break;*/
        }
    }

    @Override
    public void onBackPressed() {
        if (webView.canGoBack()) webView.goBack(); // goBack()表示返回WebV
        else finish();
    }

    @Override//防止内存泄漏
    protected void onDestroy() {
        super.onDestroy();
        webView.removeAllViews();
        webView.destroy();
    }

    @Override//防止内存泄漏
    public void finish() {
        ViewGroup view = (ViewGroup) getWindow().getDecorView();
        view.removeAllViews();
        webView.stopLoading();
        webView.getSettings().setJavaScriptEnabled(false);
        webView.clearHistory();
        webView.removeAllViews();
        webView.destroy();
        super.finish();
    }
}
