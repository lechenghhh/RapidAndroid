package com.lc.hello.Activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.lc.hello.R;
import com.lc.hello.adapter.LvUnitAdpt;
import com.lc.hello.adapter.ViewHolder;
import com.lc.hello.utils.MySP;
import com.lc.hello.utils.widgets.LoadListView;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class Fragment1 extends Fragment {

    @Bind(R.id.lv1)
    LoadListView lv1;
    List<String> list = new ArrayList<>();
    @Bind(R.id.tv1)
    TextView tv1;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_1, null, false);
        ButterKnife.bind(this, root);

        for (int i = 0; i < 10; i++)
            list.add("联系人" + i);
        lv1.setAdapter(new LvUnitAdpt<String>(getActivity(), list, R.layout.item_1) {
            @Override
            public void convert(ViewHolder helper, final String item, int position) {
                helper.setText(R.id.tv1, item);
                helper.setOnClickListener(R.id.iv2, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        MySP.set(getActivity(), "hahahaha", item);
                        Toast.makeText(getActivity(), "点击了向右", Toast.LENGTH_SHORT).show();
                        tv1.setText(MySP.get(getActivity(), "hahahaha", ""));
                    }
                });
                helper.setOnClickListener(R.id.tvDelete, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Toast.makeText(getActivity(), "删除了一个条目", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
}
