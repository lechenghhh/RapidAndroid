package com.lc.hello.Activity.unit;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.lc.hello.R;
import com.lc.hello.utils.MyApplication;
import com.lc.hello.utils.MyUtils;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

/*支持功能：
 * 拍照返回照片路径
 * 自动创建右上角菜单
 * 申请相机权限*/
public class BaseActivity extends AppCompatActivity {
    private ProgressDialog pDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActionBar actionBar = this.getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        pDialog = new ProgressDialog(this);
        pDialog.setMessage("请稍后");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                startActivity(new Intent(this, OtherActivity.class));
                return true;
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            //申请WRITE_EXTERNAL_STORAGE权限
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, 22);
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {    //拍照上传
        super.onActivityResult(requestCode, resultCode, data);
        System.out.println("CameraActivity:requestCode" + requestCode + "resultCode" + resultCode);
        String srcPath = "";
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case 1:
                    Bundle extras = data.getExtras();
                    Bitmap b = (Bitmap) extras.get("data");
                    String name = new SimpleDateFormat("yyyyMMddhhmmss").format(new Date());
                    String filePath = MyApplication.FILE_PATH + name + ".jpg";
                    srcPath = filePath;
                    System.out.println("QDAty:保存路径1----------:" + filePath);
                    File myCaptureFile = new File(filePath);
                    try {
                        if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
                            if (!myCaptureFile.getParentFile().exists())
                                myCaptureFile.getParentFile().mkdirs();
                            BufferedOutputStream bos;
                            bos = new BufferedOutputStream(new FileOutputStream(myCaptureFile));
                            b.compress(Bitmap.CompressFormat.JPEG, 80, bos);
                            bos.flush();
                            bos.close();
//                            btn1.setText("路径: " + srcPath);
//                            ImageLoader.getInstance().displayImage("file:///" + srcPath, ivHead);
                            onActivityResultWithPath(srcPath);
                        } else {
                            Toast.makeText(this, "照片保存失败", Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case 2:
                    srcPath = MyUtils.getPath(this, data.getData());//Uri转Url
                    System.out.println("QDAty:Uri:" + data.getData() + "\n保存路径2----------:" + srcPath);
                    onActivityResultWithPath(srcPath);

                    //                    btn1.setText("路径: " + srcPath);
                    break;
            }
        }
    }

    protected void onActivityResultWithPath(String srcPath) {
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 22) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission Granted
            } else {                // Permission Denied
                //  displayFrameworkBugMessageAndExit();
                Toast.makeText(this, "请在应用管理中打开“相机”访问权限！", Toast.LENGTH_LONG).show();
                finish();
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (pDialog != null) pDialog.dismiss();
    }
}
