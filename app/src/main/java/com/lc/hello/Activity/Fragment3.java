package com.lc.hello.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;
import butterknife.OnClick;

import com.lc.hello.Activity.unit.BrowserActivity;
import com.lc.hello.Activity.unit.CameraActivity;
import com.lc.hello.R;

public class Fragment3 extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_3, null, false);
        ButterKnife.bind(this, root);
        return root;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @OnClick({R.id.btnBrowser, R.id.btnTake})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnTake:
                startActivity(new Intent(getActivity(), CameraActivity.class));
                break;
            case R.id.btnBrowser:
                startActivity(new Intent(getActivity(), BrowserActivity.class));
                break;
        }
    }
}
