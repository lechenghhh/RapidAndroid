package com.lc.hello.utils;

import android.content.Context;
import android.content.Intent;


public class ShareToFriends {

    public ShareToFriends(Context context, String text) {
        Intent intent = new Intent(Intent.ACTION_SEND); //
        intent.setType("text/plain"); //
        intent.putExtra(Intent.EXTRA_SUBJECT, "111"); //
        intent.putExtra(Intent.EXTRA_TEXT, text); //
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(Intent.createChooser(intent, "分享一下"));//对话框标题
    }
}
