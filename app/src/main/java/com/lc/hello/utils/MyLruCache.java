package com.lc.hello.utils;

import android.util.LruCache;

public class MyLruCache<T> extends LruCache<String, T> {

    private static volatile MyLruCache instance = null;

    private MyLruCache(int maxSize) {
        super(maxSize);
    }

    public static <T> MyLruCache getInstance() {
        if (instance == null) {
            synchronized (MyLruCache.class) {
                if (instance == null) {
                    instance = new MyLruCache<T>(1024 * 1024 * 20);
                }
            }
        }
        return instance;
    }
}

/* 使用方法:

MyLruCache<Bitmap> myLruCache = MyLruCache.getInstance();

Bitmap thatBitmap = myLruCache.get("key");
if (thatBitmap == null) {
    thatBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.bg_047);
    myLruCache.put("key", thatBitmap);
}
iv1.setImageBitmap(thatBitmap);

---------------------
作者：燕小溜
来源：CSDN
原文：https://blog.csdn.net/sinat_30352293/article/details/83410839
版权声明：本文为博主原创文章，转载请附上博文链接！*/